// Evol functions.
// Author:
//    Reid
// Description:
//    Tell a random sentence.
// Variables:
//    .@rand = Random number between the number of sentence choice.

function	script	sailortalk	{

    .@rand = rand(8);
    if (.@rand == 0) goodbye;
    if (.@rand == 1) npctalkonce(l("Ah, this is boring."));
    if (.@rand == 2) npctalkonce(l("Good to hear from you!"));
    if (.@rand == 3) npctalkonce(l("So finally someone has came to visit me?"));
    if (.@rand == 4)
    {
        speech(
            l("A sunny and hot day,"),
            l("a quiet place,"),
            l("a ground!"),
            l("What else do you need?"));
        close;
    }
    if (.@rand == 5) npctalkonce(l("A-hoy matey!"));
    if (.@rand == 6) npctalkonce(l("Arr!"));
    if (.@rand == 7) npctalkonce(l("Howdy?"));

    // just to be sure
    closedialog;
    close;
    end;
}
