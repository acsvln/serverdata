// Evol functions.
// Author:
//    4144
// Description:
//    Function checking the client version and reports if it is too old.

function	script	checkclientversion	{
    if (ClientVersion > 20) return;

    mesn "Narrator";
    mes col("Warning.", 1);
    mes col("Warning.", 1);
    mes col("Warning: You are using an old client.", 9);
    next;
    mes col("Please install the new client from [@@https://manaplus.org/|https://manaplus.org/@@]", 9);
    next;
    mes col("You won't be allowed to play this game until you have a valid client.", 9);
    next;
    // Force user to disconnect
    atcommand "@kick "+strcharinfo(0);
    return;
}
