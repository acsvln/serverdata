function	script	SoulMenhir	{
    mes l("[Soul Menhir]");
    mes l("(A mystical aura surrounds this stone. You feel mysteriously attracted to it. Something tells you to touch it. What do you do?)");

    menu
        l("Touch it."), L_Bind,
        rif($@GM_EVENT, l("Send soul to the Mana Plane for GM events")), L_Aeros,
        rif($EVENT$ == "Easter", l("[Easter] Send soul to the Mana Forest!")), L_Easter,
        l("Leave it alone."), L_Return;

//L_Towel:
//    if (TowelLastUsed > (gettimetick(2) - 1800))
//        goto L_DontPanic;
//    TowelLastUsed = gettimetick(2);
//    mes l("[Soul Menhir]");
//    mes "(You touch the mysterious stone. Somehow it feels hard and soft at the same time.)";
//    getitem "HitchhikersTowel", 1;
//    goto L_Return;

L_Bind:
    if (Menhir_Activated == 1)
        goto L_Shortversion;

    mes l("[Soul Menhir]");
    mes l("(You touch the mysterious stone. Somehow it feels warm and cold at the same time.)");
    mes l("(Suddenly a strange sensation flows through you. It feels like your soul leaves your body and becomes one with the stone.)");
    mes l("(As suddenly as the feeling started it stops. The strange attraction is away from one moment to the next and the menhir feels like just an ordinary stone.)");
    Menhir_Activated = 1;
    goto L_Save;

L_Shortversion:
    mes l("[Soul Menhir]");
    mes l("(A strange sensation flows through you. It feels like your soul leaves your body and becomes one with the stone. As suddenly as the feeling started it stops.)");
    goto L_Save;

L_Save:
    if (@x == 0 && @y == 0)
        goto L_FindPoint;
    goto L_Do_Save;

L_Aeros:
    if (!$@GM_EVENT) goto L_DontPanic;
    warp "001-1", 235, 26;
    message strcharinfo(0), l("You are now at the Mana Plane of Existence, at the Floating Island of Aeros.");
    close;

L_Easter:
    if ($EVENT$ != "Easter") goto L_DontPanic;
    warp "001-4", 151, 157;
    message strcharinfo(0), l("You are now at the Mana Plane of Existence, at the Magical Forest.");
    close;

L_DontPanic:
    message strcharinfo(0), l("(A strange barrier keeps you from touching the stone at this time.)");
    goto L_Return;

L_Do_Save:
    savepoint @map$, @x, @y;
    specialeffect(4, SELF, getcharid(3));

    // As we have a dialog box open, this function produces an undesirable player talk.
    //savepointparticle @map$, @x, @y, NO_INN;
    goto L_Return;

L_FindPoint:
    @n = rand(getarraysize(@Xs));
    @x = @Xs[@n];
    @y = @Ys[@n];
    goto L_Do_Save;

L_Return:
    return;
}
