// TMW2 Scripts.
// Author:
//    Jesusalva
// Description:
//    Doors NPCs.

002-1,72,30,0	script	AreaNPC#002-1d	NPC_HIDDEN,0,0,{

OnTouch:
    if (LOCATION$ == "") 
        warp "002-3", 42, 26;
    else
        warp "002-3@"+LOCATION$, 42, 26;
    close;
}
