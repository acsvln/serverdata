// Evol scripts.
// Author:
//    Reid
// Description:
//    Blacksmith's assistant of Artis
// Variables:
//    ArtisQuests_Enora
// Values:
//    0    Default.
//    1    BlackSmith quest delivered.
//    2    Chelios Quest given.
//    3    Chelios Quest done.
//    4    BlackSmith gave the sword.

test,2,6,0	script	rock	NPC_PLAYER,{

    function quest_play {
        mes l("Before start witch item do you want to play");        
    mes "##B" + l("Drag and drop an item from your inventory.") + "##b";

    .Item = requestitem();
    if (.Item < 1)
    {
        mes l("You didn't add a item.");
        close;
    }

    if (countitem(.Item) < 1)
    {
        mes l("You don't have the item.");
        close;
    }
    delitem .Item, 1;
    goto quest_choose;
    }

    function quest_game {
        if (.choose == 1){
            mes l("you choose rock.");
            next;
        }
        else if (.choose == 2){
            mes l("you choose scissors.");
            next;
        }
        else  {
            mes l("you choose paper.");
            next;
        }

        .npcChoose = rand(3);
        if (.npcChoose == 0)
        {
            mes l("the npc choose rock.");
            next;
            if (.choose == 1)
            {
                mes l("draw.");
                goto quest_choose;
            }
            else if (.choose == 2)
            {
                mes l("you lose");
                close;
            }
            else if (.choose == 3)
            {
                mes l("you win");
                getitem .Item, 2;
                close;
            }
        }
        else if (.npcChoose == 1)
        {
            mes l("the npc choose scissors.");
            next;
            if (.choose == 2)
            {
                mes l("draw.");
                goto quest_choose;
            }
            else if (.choose == 3)
            {
                mes l("you lose");
                close;
            }
            else if (.choose == 1)
            {
                mes l("you win @@",getitemlink(.Item));
                getitem .Item, 2;
                close;
            }
        }
        else if (.npcChoose == 2)
        {
            mes l("the npc choose paper.");
            next;
            if (.choose == 3)
            {
                mes l("draw.");
                goto quest_choose;
            }
            else if (.choose == 1)
            {
                mes l("you lose");
                close;
            }
            else if (.choose == 2)
            {
                mes l("you win");
                getitem .Item, 2;
                close;
            }
        }
        close;
    }


    function quest_choose {
        speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
            l("What do you choose?");
            next;
        do
        {
            select
                l("Rock"),
                l("Scissors"),
                l("Paper");

            switch (@menu)
            {
                case 1:
                    .choose = 1;
                    quest_game;
                    break;
                case 2:
                    .choose = 2;
                    quest_game;
                    break;
                case 3:
                    .choose = 3;
                    quest_game;
                    break;
            }
        } while (@menu != 4);
        close;
    }

    speech S_LAST_NEXT, l("Hello do you want to play rock scissors paper?");
    do
    {
        select
            l("Hello"),
            menuaction(l("Quit"));

        switch (@menu)
        {
            case 1:
                quest_play;
                break;
        }
    } while (@menu != 2);

    closedialog;
    goodbye;
    close;

OnInit:
    .sex = G_MALE;
    .distance = 3;
    end;
}

