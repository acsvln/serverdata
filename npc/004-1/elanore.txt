// TMW-2 Script
// Author:
//    Jesusalva

004-1,75,62,0	script	Elanore the Healer	NPC_FEMALE,{

    //if (sc_check(SC_POISON)) goto L_CurePoison;

    mesn;
    @temp = rand(4);
    if(@temp == 1) goto L_Heal2;
    if(@temp == 2) goto L_Heal3;
    if(@temp == 3) goto L_Heal4;
    goto L_Heal1;

L_Heal1:
    mesq l("You don't look too well; let me treat your wounds.");
    mes "";
    goto L_Menu;

L_Heal2:
    mesq l("I will make quick work of your wounds.");
    mes "";
    goto L_Menu;

L_Heal3:
    mesq l("Need a healing?");
    mes "";
    goto L_Menu;

L_Heal4:
    mesq l("Sometimes you just need to run from battle.");
    mes "";
    goto L_Menu;

L_Menu:
    .@price=(MaxHp-Hp)/5;
    .@price=.@price+10;
    if (BaseLevel <= 15) .@price=(.@price/10);
    mesq l("For you, it'll be @@ GP.", .@price);
    mes "";
    menu
        rif(Zeny >= .@price, l("Please heal me!")), L_Heal_L,
        rif(BaseLevel > 15 && 1==2, l("Do you make Lifestones?")), L_Lifestones,
        l("Another time, maybe."), L_Close;


L_Heal_L:
    mes "";
    set Zeny, Zeny - .@price;
    sc_end(SC_POISON);
    sc_end(SC_SLOWPOISON);
    percentheal 100,100; // We can also use "recovery(<account id>)"
    mesn;
    @temp = rand(1,4);
    if(@temp == 1)    mesq l("Here you go!");
    if(@temp == 2)    mesq l("Painless, wasn't it?");
    if(@temp == 3)    mesq l("You should be more careful.");
    if(@temp == 4)    mesq l("Much better, right?!");
    goto L_Close;

L_Lifestones:
    mes "";
    mesn;
    mesq l("Lifestones have the potential to heal the user, and thus are used for potions.");
    next;
    mesq l("I will need 1 @@, 1 @@ and 100 GP to craft a single stone.", getitemlink(BugLeg), getitemlink(MaggotSlime));
    next;
    mes "";
    menu
        l("OK, I will get them."), L_Close,
        l("No way!"), L_Close,
        l("Here you are!"), L_Trade;

L_Trade:
    if (countitem("BugLeg") < 1) goto L_Trade_Missing;
    if (countitem("MaggotSlime") < 1) goto L_Trade_Missing;
    if (Zeny < 100) goto L_Trade_Missing;

    // TODO: There is a function to do that check in one line
    getinventorylist;
    if (@inventorylist_count == 100 && countitem("Lifestone") == 0
        && countitem("BugLeg") > 1
        && countitem("MaggotSlime") > 1) goto L_NoRoom;

    delitem "BugLeg", 1;
    delitem "MaggotSlime", 1;
    set Zeny, Zeny - 100;

    // TODO: MISSING ITEM
    //getitem "Lifestone", 1;

    // TODO: Only grant Experience on first craft
    getexp 80, 0;
    mesn;
    mesq l("These will do just fine.");
    next;
    mesn;
    mes l("Elanore casts a chant over the items, closes her hands, and vóila! A lifestone.");
    next;
    mesq l("I guess that you could use these lifestones to gain a regeneration effect, too, but I do not know how.");
    next;
    mes l("She smiles.");
    mes l("[80 experience points]");
    goto L_Close;

L_NoRoom:
    mesn;
    mesq l("Oh... it seems that you have no room for the lifestones. But please do come back later when you have made a little space.");
    goto L_Close;

L_Trade_Missing:
    mesn;
    mesq l("This is not what I asked for.");
    goto L_Close;

L_Close:
    close;

OnInit:
    .@npcId = getnpcid(0, .name$);
    //setunitdata(.@npcId, UDT_HEADTOP, 2929);
    setunitdata(.@npcId, UDT_HEADMIDDLE, 1317);
    //setunitdata(.@npcId, UDT_HEADBOTTOM, 2207);
    setunitdata(.@npcId, UDT_WEAPON, 1802); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 8);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 17);

    .sex = G_FEMALE;
    .distance = 5;
    end;

}
