// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Soul Menhir

005-1,29,92,0	script	Soul Menhir#candor	NPC_SOUL_MOSS,{
    @map$ = "005-1";
    setarray @Xs, 27, 28, 27, 28, 27, 28, 28;
    setarray @Ys, 91, 92, 93, 91, 92, 93, 92;
    @x = 0;
    @y = 0;
    callfunc "SoulMenhir";
    @map$ = "";
    cleararray @Xs[0], 0, getarraysize(@Xs);
    cleararray @Ys[0], 0, getarraysize(@Ys);
    @x = 0;
    @y = 0;
    close;
}
