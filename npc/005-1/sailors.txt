// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    TODO
// CandorQuest_Sailors
//  0: Not started
//  1: Accepted
//  2: Invite Elmo
//  3: Completed

005-1,95,99,0	script	Sailors#005-1	NPC_ELVEN_MAN_TRADER_SITTING,0,0,{
    .@q = getq(CandorQuest_Sailors);
    if (.@q == 1)
        goto L_Report;
    if (.@q == 2)
        goto L_Elmo;
    if (.@q == 3)
        goto L_Complete;

    mesn;
    mesq l("Ahoy matey!");
    next;
    mesq l("Arr, it is always good to be on land after so much time in sea!");
    next;
    mesq l("We want to party this moment, but can you believe we ran out of beer?");
    mes "";
    menu
        l("If I were you, I would drink water."),L_Water,
        l("WHAT? How can you ever do a party without beer?!"),L_Accept;

L_Water:
    mes "";
    mesn;
    mesq l("Ah, it is not the same. Not the same.");
    next;
    mesq l("When I am drunk I tell myself to stop drinking, but I won't listen the words of a drunkard.");
    close;

L_Accept:
    mes "";
    mesn;
    mesq l("We can't, don't you agree?!");
    next;
    mesq l("However, if we waste the ship's money in beer, Nard will get mad.");
    next;
    mesq l("We won't be able to pay you in money, but we'll make you one of us if you bring us beer!");
    next;
    mesq l("Please bring us 5 @@! That should be enough!", getitemlink("Beer"));
    setq CandorQuest_Sailors, 1;
    close;

L_Report:
    mesn;
    mesq l("I see you brought @@/5 @@ for us!",countitem("Beer"),getitemlink("Beer"));
    mes "";
    menu
        rif(countitem("Beer") >= 5, l("Indeed, matey! Here they are!")), L_Give,
        rif(countitem("Beer") >= 5, l("You're doing the math wrong, matey! I'll bring them later!")), L_Later,
        rif(countitem("Beer") < 5, l("Arr, that's not enough! I'll bring more later!")), L_Later;
    close;

L_Later:
    mes "";
    mesn;
    mesq l("Arr, we will wait for you then! We still have tasks to complete!");
    close;

L_Give:
    inventoryplace Bandana, 1;
    delitem "Beer", 5;
    getitem Bandana, 1;
    setq CandorQuest_Sailors, 2;
    mes "";
    mesn;
    mesq l("Arr, that's some fine ale! We can do the party when we're done with our work!");
    next;
    mesn;
    mesq l("Take this @@ to prove you're one of us! Could you also invite Elmo? Thanks, matey!", getitemlink("Bandana"));
    close;


L_Elmo:
    mesn;
    mesq l("Please invite Elmo for the party, matey! We can't leave our positions!");
    close;

L_Complete:
    mesn;
    mesq l("Thanks for the help! Arr, that was some fine ale, indeed!");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 8;
    end;
}
